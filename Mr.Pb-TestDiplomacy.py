#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py


# -------
# imports
# -------
from unittest import main, TestCase
from io import StringIO
import io
from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_read, diplomacy_solve




class TestDiplomacy(TestCase):
    '''def setUp(self):
        # Redirect logging to a StringIO for testing
        self.log_output = StringIO()
        import logging
        logging.basicConfig(stream=self.log_output, level=logging.DEBUG)

    def tearDown(self):
        # Clean up log output
        self.log_output.close())'''

    def test_diplomacy_eval_0(self):
        input_lines = ["A Madrid Hold", "B Barcelona Hold"]
        expected_output = {'A': 'Madrid', 'B': 'Barcelona'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)


    def test_diplomacy_eval_1(self):
        input_lines = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Austin Support A"]
        expected_output = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': 'Paris', 'E': 'Austin'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)

    def test_diplomacy_eval_2(self):
        # Test a case with Move actions
        input_lines = ["A Madrid Move Paris", "B London Move Paris"]
        expected_output = {'A': '[dead]', 'B': '[dead]'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)

    def test_diplomacy_eval_3(self):
        input_lines = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid"]
        expected_output = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)

    def test_diplomacy_eval_4(self):
        input_lines = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"]
        expected_output = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': '[dead]'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)

    def test_diplomacy_eval_5(self):
        input_lines = ["A Madrid Hold", "B Barcelona Move Madrid"]
        expected_output = {'A': '[dead]', 'B': '[dead]'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)

    def test_diplomacy_eval_6(self):
        input_lines = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"]
        expected_output = {'A': '[dead]', 'B': 'Madrid', 'C': 'London'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)

    def test_diplomacy_eval_7(self):
        input_lines = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B"]
        expected_output = {'A': '[dead]', 'B': 'Madrid', 'C': '[dead]', 'D': 'Paris'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)


    def test_diplomacy_eval_8(self):
        # treason test case
        input_lines = ["A Paris Hold", "B Paris Support A"]
        expected_output = {'A': '[dead]', 'B': '[dead]'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)

    def test_diplomacy_eval_9(self):
        # Test multiple armies at the same location with no support
        input_lines = ["A Paris Hold", "B Paris Hold"]
        expected_output = {'A': '[dead]', 'B': '[dead]'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)

    def test_diplomacy_eval_9(self):
        # Test multiple armies at the same location with no support
        input_lines = ["A Paris None"]
        expected_output = {'A': None}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)

    def test_diplomacy_eval_10(self):
        # Test multiple armies at the same location with no support
        input_lines = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Paris Support B"]
        expected_output ={'A': '[dead]', 'B': 'Madrid', 'C': 'London', 'D': 'Paris'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)

    def test_diplomacy_eval_11(self):
        # Test multiple armies at the same location with no support
        input_lines = ["A Paris Support B"]
        expected_output = {'A': 'Paris'}
        self.assertEqual(diplomacy_eval(input_lines), expected_output)


    def test_diplomacy_solve_1(self):
        r = StringIO('A Madrid Hold\nB Barcelona Hold')
        w = StringIO()
        diplomacy_solve(r, w)
        expected_output = "A Madrid\nB Barcelona\n"
        self.assertEqual(w.getvalue(), expected_output)

    def test_diplomacy_solve_2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
        w = StringIO()
        diplomacy_solve(r, w)
        expected_output = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
        self.assertEqual(w.getvalue(), expected_output)

    def test_diplomacy_solve_3(self):
        r = StringIO('A Madrid Move Paris\nB London Move Paris')
        w = StringIO()
        diplomacy_solve(r, w)
        expected_output = "A [dead]\nB [dead]\n"
        self.assertEqual(w.getvalue(), expected_output)

    def test_diplomacy_solve_4(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        expected_output = "A [dead]\nB [dead]\nC [dead]\n"
        self.assertEqual(w.getvalue(), expected_output)

    def test_diplomacy_print(self):
        # Test diplomacy_print function with a mock output_writer
        armies = {'A': 'Paris', 'B': 'London'}
        output_writer = StringIO()
        diplomacy_print(armies, output_writer)
        expected_output = "A Paris\nB London\n"
        self.assertEqual(output_writer.getvalue(), expected_output)

    def test_diplomacy_read(self):
        # Test diplomacy_read function with a mock input_string
        input_string = "A Paris Hold\nB London Move Paris\n"
        expected_output = ["A Paris Hold", "B London Move Paris"]
        self.assertEqual(diplomacy_read(input_string), expected_output)



if __name__ == '__main__':
    main()