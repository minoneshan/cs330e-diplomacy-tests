from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_print, diplomacy_solve, diplomacy_read, diplomacy_help

class TestDiplomacy(TestCase):
    def test_read_1(self):
        s = "A London Move Portland\n"
        row = diplomacy_read(s)
        self.assertEqual(row, ['A', 'London', 'Move', 'Portland'])

    def test_read_2(self):
        s = "B Austin Hold\n"
        row = diplomacy_read(s)
        self.assertEqual(row, ['B', 'Austin', 'Hold'])

    def test_read_3(self):
        s = "C Support A\n"
        row = diplomacy_read(s)
        self.assertEqual(row, ['C', 'Support', 'A'])

    def test_solve_1(self):
        r = StringIO("A London Move Portland\nB Boston Support A\nC Madrid Move Portland\nD Paris Support C\nE Austin Move Paris\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Portland\nB Boston\nC [dead]\nD [dead]\nE [dead]\n")

    def test_solve_2(self):
        r = StringIO("A London Hold\nB Boston Support A\nC Madrid Move London\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A London\nB Boston\nC [dead]\n")

    def test_solve_3(self):
        r = StringIO("D Austin Move London\nB London Support A\nC Madrid Move Portland\nA Paris Move Portland\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w,{'A':'[dead]'})
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w,{'B':'Madrid'})
        self.assertEqual(w.getvalue(), "B Madrid\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w,{'C':'Paris'})
        self.assertEqual(w.getvalue(), "C Paris\n")

if __name__ == "__main__": #pragma: no cover
    main()
