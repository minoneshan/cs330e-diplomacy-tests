from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
# ----
    # read
    # ----

    #def test_read_1(self):
    #    s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A"
    #    a = diplomacy_read(s)
        
        #self.assertEqual(a, 'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
    #    self.assertEqual(a, ['A Madrid Hold','B Barcelona Move Madrid','C London Move Madrid','D Paris Support B','E Austin Support A'])
        #self.assertEqual(b, 'B Barcelona Move Madrid')
        #self.assertEqual(c, 'C London Move Madrid')
        #self.assertEqual(d, 'D Paris Support B')
        #self.assertEqual(e, 'E Austin Support A')
    

    def test_eval_1(self):
        v = diplomacy_eval(['A Orlando Hold','B Miami Move Orlando','C Premont Support B','D LosAngeles Move Premont'])
        self.assertEqual(v, ['A [dead]','B [dead]','C [dead]','D [dead]'])
        #self.assertEqual(a1, 'A [dead]')
        #self.assertEqual(b1, 'B [dead]')
        #self.assertEqual(c1, 'C [dead]')
        #self.assertEqual(d1, 'D Paris')
        #self.assertEqual(e1, 'E Austin')
    #def test_eval_2(self):
    #    v = collatz_eval(100, 200)
    #    self.assertEqual(v, 125)

    #def test_eval_3(self):
    #    v = collatz_eval(201, 210)
    #    self.assertEqual(v, 89)


    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]','B Paris'])
        self.assertEqual(w.getvalue(), 'A [dead]\nB Paris\n')

    

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("H Vienna Move Orlando\nI RioDeJaneiro Support H\nJ Savannah Move Vienna\nK Lincoln Support L\nL Havana Hold\nM Santiago Move Havana\nN York Hold\nO Berlin Support J\nP London Suppoer J\nQ Brooklyn Move London\nR Rome Support J\nS Madison Hold\nU Florence Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "H Orlando\nI RioDeJaneiro\nJ Vienna\nK Lincoln\nL [dead]\nM [dead]\nN York\nO Berlin\nP [dead]\nQ [dead]\nR Rome\nS Madison\nU Florence\n")

    def test_solve_2(self):
        r = StringIO("M SanDiego Hold\nN ElPaso Move SanDiego\nO Chicago Support M\nP Miami Move SanDiego\nQ Orlando Hold\nR Albany Support N\nS Denver Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "M [dead]\nN [dead]\nO Chicago\nP [dead]\nQ Orlando\nR Albany\nS Denver\n")

    def test_solve_3(self):
        r = StringIO("D Paris Move Houston")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "D Houston\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()