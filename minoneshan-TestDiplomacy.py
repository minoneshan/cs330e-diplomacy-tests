#!/usr/bin/env python3

# ----------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Liu/Minoneshan
# ----------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read_input, diplomacy_process_actions, diplomacy_print_results

# ---------------
# TestDiplomacy
# ---------------

class TestDiplomacy(TestCase):
    # -----------------
    # read_input
    # -----------------

    def test_read_input_1(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n"
        actions = diplomacy_read_input(s)
        self.assertEqual(actions, ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"])

    def test_read_input_2(self):
        s = "A Madrid Hold\n"
        actions = diplomacy_read_input(s)
        self.assertEqual(actions, ["A Madrid Hold"])

    def test_read_input_3(self):
        s = ""
        actions = diplomacy_read_input(s)
        self.assertEqual(actions, [])
    # -----------------------
    # process_actions
    # -----------------------

    def test_process_actions_1(self):
        actions = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"]
        results = diplomacy_process_actions(actions)
        expected_results = ["A [dead]", "B [dead]", "C [dead]", "D [dead]"]
        self.assertEqual(results, expected_results)

    def test_process_actions_2(self):
        actions = ["A Madrid Hold"]
        results = diplomacy_process_actions(actions)
        expected_results = ["A Madrid"]
        self.assertEqual(results, expected_results)

    def test_process_actions_3(self):
        actions = ["A Madrid Hold", "B Barcelona Move Madrid"]
        results = diplomacy_process_actions(actions)
        expected_results = ["A [dead]", "B [dead]"]
        self.assertEqual(results, expected_results)


    def test_process_actions_4(self):
        actions = ["A Madrid Hold", "B Barcelona Move Madrid","C London Move Madrid","D Paris Support B","E Austin Support A"]
        results = diplomacy_process_actions(actions)
        expected_results = ["A [dead]", "B [dead]", "C [dead]", "D Paris", "E Austin"]
        self.assertEqual(results, expected_results)


    def test_process_actions_5(self):
        actions = ["A Madrid Support B"]
        results = diplomacy_process_actions(actions)
        expected_results = ["A Madrid"]
        self.assertEqual(results, expected_results)

    def test_process_actions_6(self):
        actions = ["A Madrid Move London","B Barcelona Move London"]
        results = diplomacy_process_actions(actions)
        expected_results = ["A [dead]", "B [dead]"]
        self.assertEqual(results, expected_results)

    def test_process_actions_7(self):
        actions = ["A Madrid Hold","B Barcelona Move Madrid",'C Paris Support A','D Austin Support A','E London Move Madrid']
        results = diplomacy_process_actions(actions)
        expected_results = ["A Madrid", "B [dead]",'C Paris','D Austin', 'E [dead]']
        self.assertEqual(results, expected_results)

    def test_process_actions_8(self):
        actions = []
        results = diplomacy_process_actions(actions)
        expected_results = []
        self.assertEqual(results, expected_results)



    # -------------------------
    # print_results
    # -------------------------

    def test_print_results_1(self):
        w = StringIO()
        results = ["A Madrid", "C London", "D Austin"]
        diplomacy_print_results(w, results)
        self.assertEqual(w.getvalue(), "A Madrid\nC London\nD Austin\n")

    def test_print_result_2(self):
        w = StringIO()
        results = ["A Madrid"]
        diplomacy_print_results(w, results)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_result_3(self):
        w = StringIO()
        results = []
        diplomacy_print_results(w, results)
        self.assertEqual(w.getvalue(), "")

# ----
# main
# ----

if __name__ == "__main__":
    main()
